//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem.
// Gerado em: 2021.07.23 às 11:06:17 AM BRT
//

@javax.xml.bind.annotation.XmlSchema(
        namespace = "http://ws.speedgov.com.br/tipos_v1.xsd",
        elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED,
        xmlns = {
            @XmlNs(prefix="elre", namespaceURI="http://ws.speedgov.com.br/enviar_lote_rps_envio_v1.xsd"),
            @XmlNs(prefix="clre", namespaceURI="http://ws.speedgov.com.br/consultar_lote_rps_envio_v1.xsd"),
            @XmlNs(prefix="p1", namespaceURI="http://ws.speedgov.com.br/tipos_v1.xsd"),
            @XmlNs(prefix="ds", namespaceURI="http://www.w3.org/2000/09/xmldsig#"),
            @XmlNs(prefix="xsi", namespaceURI="http://www.w3.org/2001/XMLSchema-instance"),
        }
)
package br.org.abrasf.nfse1_0;

import javax.xml.bind.annotation.XmlNs;
