//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2021.07.23 às 11:06:17 AM BRT 
//


package br.org.abrasf.nfse1_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de tcConfirmacaoCancelamento complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="tcConfirmacaoCancelamento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Pedido" type="{http://ws.speedgov.com.br/tipos_v1.xsd}tcPedidoCancelamento"/>
 *         &lt;element name="InfConfirmacaoCancelamento" type="{http://ws.speedgov.com.br/tipos_v1.xsd}tcInfConfirmacaoCancelamento"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Id" type="{http://ws.speedgov.com.br/tipos_v1.xsd}tsIdTag" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tcConfirmacaoCancelamento", propOrder = {
    "pedido",
    "infConfirmacaoCancelamento"
})
public class TcConfirmacaoCancelamento {

    @XmlElement(name = "Pedido", required = true)
    protected TcPedidoCancelamento pedido;
    @XmlElement(name = "InfConfirmacaoCancelamento", required = true)
    protected TcInfConfirmacaoCancelamento infConfirmacaoCancelamento;
    @XmlAttribute(name = "Id")
    protected String id;

    /**
     * Obtém o valor da propriedade pedido.
     * 
     * @return
     *     possible object is
     *     {@link TcPedidoCancelamento }
     *     
     */
    public TcPedidoCancelamento getPedido() {
        return pedido;
    }

    /**
     * Define o valor da propriedade pedido.
     * 
     * @param value
     *     allowed object is
     *     {@link TcPedidoCancelamento }
     *     
     */
    public void setPedido(TcPedidoCancelamento value) {
        this.pedido = value;
    }

    /**
     * Obtém o valor da propriedade infConfirmacaoCancelamento.
     * 
     * @return
     *     possible object is
     *     {@link TcInfConfirmacaoCancelamento }
     *     
     */
    public TcInfConfirmacaoCancelamento getInfConfirmacaoCancelamento() {
        return infConfirmacaoCancelamento;
    }

    /**
     * Define o valor da propriedade infConfirmacaoCancelamento.
     * 
     * @param value
     *     allowed object is
     *     {@link TcInfConfirmacaoCancelamento }
     *     
     */
    public void setInfConfirmacaoCancelamento(TcInfConfirmacaoCancelamento value) {
        this.infConfirmacaoCancelamento = value;
    }

    /**
     * Obtém o valor da propriedade id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
