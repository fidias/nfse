//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem.
// Gerado em: 2021.07.23 às 11:06:17 AM BRT
//


package br.org.abrasf.nfse1_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 *
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoteRps" type="{http://ws.speedgov.com.br/tipos_v1.xsd}tcLoteRps"/>
 *         &lt;element ref="{http://www.w3.org/2000/09/xmldsig#}Signature" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "loteRps",
    "signature"
})
@XmlRootElement(name = "EnviarLoteRpsEnvio", namespace = "http://ws.speedgov.com.br/enviar_lote_rps_envio_v1.xsd")
public class EnviarLoteRpsEnvio {

    @XmlElement(name = "LoteRps", namespace = "http://ws.speedgov.com.br/enviar_lote_rps_envio_v1.xsd", required = true)
    protected TcLoteRps loteRps;
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#")
    protected SignatureType signature;

    /**
     * Obtém o valor da propriedade loteRps.
     *
     * @return
     *     possible object is
     *     {@link TcLoteRps }
     *
     */
    public TcLoteRps getLoteRps() {
        return loteRps;
    }

    /**
     * Define o valor da propriedade loteRps.
     *
     * @param value
     *     allowed object is
     *     {@link TcLoteRps }
     *
     */
    public void setLoteRps(TcLoteRps value) {
        this.loteRps = value;
    }

    /**
     * Obtém o valor da propriedade signature.
     *
     * @return
     *     possible object is
     *     {@link SignatureType }
     *
     */
    public SignatureType getSignature() {
        return signature;
    }

    /**
     * Define o valor da propriedade signature.
     *
     * @param value
     *     allowed object is
     *     {@link SignatureType }
     *
     */
    public void setSignature(SignatureType value) {
        this.signature = value;
    }

}
