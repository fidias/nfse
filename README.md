# NFS-e

Projeto para geração e compilação das Classes Java
referentes ao projeto NFS-e (Nota Fiscal de Serviço Eletrônica).

Este projeto utiliza Maven2.

## XSD - Passo a passo

Baixar arquivos de <http://www.abrasf.org.br/paginas_multiplas_detalhes.php?cod_pagina=1&titulo=TEMAS%20T%C9CNICOS&data=nao>.

Substituir arquivos baixados no diretório `schema`.

Num terminal executar:

```bash
xjc -nv -d src/main/java/ \
    schemas/v1.0/enviar_lote_rps_envio_v1.xsd \
    schemas/v1.0/enviar_lote_rps_resposta_v1.xsd \
    schemas/v1.0/cancelar_nfse_envio_v1.xsd \
    schemas/v1.0/cancelar_nfse_resposta_v1.xsd \
    schemas/v1.0/consultar_lote_rps_envio_v1.xsd \
    schemas/v1.0/consultar_lote_rps_resposta_v1.xsd \
    schemas/v1.0/consultar_nfse_envio_v1.xsd \
    schemas/v1.0/consultar_nfse_resposta_v1.xsd \
    schemas/v1.0/consultar_nfse_rps_envio_v1.xsd \
    schemas/v1.0/consultar_nfse_rps_resposta_v1.xsd \
    schemas/v1.0/consultar_situacao_lote_rps_envio_v1.xsd \
    schemas/v1.0/consultar_situacao_lote_rps_resposta_v1.xsd \
    -p br.org.abrasf.nfse1_0
```

```bash
# Exemplo
xjc -nv -d src/main/java/ \
    schemas/4.00/distDFeInt_v1.01.xsd \
    schemas/4.00/resNFe_v1.01.xsd \
    schemas/4.00/resEvento_v1.01.xsd \
    schemas/4.00/retDistDFeInt_v1.01.xsd \
    -p br.inf.portalfiscal.nfe400.distdfe
```

1. `xjc -d src/main/java/ schemas/nfe_v3.10.xsd -p br.inf.portalfiscal.nfe`
1. `xjc -d src/main/java/ schemas/envEventoCancNFe_v1.00.xsd  -p br.inf.portalfiscal.evento.canc -b schemas-conf/000DefaultBinding.xsd`
1. `xjc -d src/main/java/ schemas/carta-correcao/envCCe_v1.00.xsd -p br.inf.portalfiscal.nfe.evento.correcao`
1. `xjc -d src/main/java/ schemas/evento-generico/retEnvEvento_v1.00.xsd -p br.inf.portalfiscal.evento.manifestacao -b schemas-conf/000DefaultBinding.xsd `

**OBS.**: Pode ser que seja necessário modificar a versão!

## WSDL

### Links

* <http://www.javac.com.br/jc/posts/list/30/1867.page> - Exemplo uso Axis2

## Passo a passo WDSL

Adaptar para cada Provedor (exemplo abaixo de conversão de WebService de NF-e)

```bash
~/dev-lib/java/axis2-1.5.6/bin/wsdl2java.sh \
    -S src/main/java/ \
    -uri wsdl/4.00/NFeAutorizacao4.wsdl
```

## Dica

Para melhor performance de validação de arquivos XML com XSD, substituir valores
altos de atributos `maxOccurs` por `unbounded`.

Em testes feitos com a NFe, o tempo de validação foi de 10s (javax.xml)
para 496 ms.

De qualquer forma o documento será validado pelo web service, assim fica melhor
para o cliente - onde a execução é mais rápida, e para a fidias - menos gastos
com CPU.
